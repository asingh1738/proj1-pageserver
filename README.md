## Author: Arjun Singh, asingh7@uoregon.edu
## Description: This software extends a tiny web server that takes a request and serves it if it is an .html or
## .css file, as long as it exists and doesn't start with a forbidden character (~,/,..)
